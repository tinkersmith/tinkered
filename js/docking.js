
(function ($, Tinkered) {
  'use strict';

  /**
   * Constructor function for creating a new dockable object.
   *
   * @param {jQuery} $elem
   *   HTML element that is being docked.
   * @param {jQuery} $container
   *   HTML element which defines the bounds.
   * @param {Object} settings
   *   Object containing the docker settings.
   *   {
   *     edge: {string} ['top'|'left'|'bottom'|'right'],
   *     collapsible: {bool} false,
   *     trackMutations: {bool} false,
   *     sticky: {Object|bool} {
   *       type: {string} [slide],
   *       // Animation will last for 200 milliseconds.
   *       duration: {int} 200,
   *       // Animation starts after 250% of the element dimension.
   *       // This value is ignored of no animatable options are enabled.
   *       // NOTE: can be also be a constant pixel value.
   *       dimBuffer: {int|string} '250%'
   *     }
   *   }
   */
  Tinkered.Docker = function ($elem, $container, settings) {
    this.elem = $elem;
    this.container = $container;
    this.config = {edge: 'bottom'};

    // If no settings provided, or explicitly specified, check CSS class
    // attributes for hints on the desired docking settings.
    if (!settings || settings.detectOpts) {
      this._parseOptions($elem.attr('class'));
    }

    var edge = this.config.edge;
    $.extend(this.config, settings || {}, Tinkered.Docker[edge]);

    // Build the docker, possibly with edge specific override function.
    this._build();

    // Register events that may make changes to docking.
    if (this.onScroll) {
      Tinkered.EventListeners.scroll.push(this);
    }
    if (this.onResize) {
      Tinkered.EventListeners.resize.push(this);
    }
  };

  Tinkered.Docker.prototype = {

    /**
     * RegExp string used to parse CSS classes for docking options and info.
     *
     * @type {String}
     */
    _optRegExPat: '(?:^|\\s)tsdock-(opt|edge)-([\\w\\-]+)(?:\\s|$)',

    /**
     * Mutation event listener. Will be registered by relevant docker types
     * and trigger when the docking element is modified in the appropriate ways.
     *
     * @param  {Mutation[]} mutation
     *  Array of mutation activity that has trigger this mutation event.
     *  Mutations are captured in a batches, and can have multiple triggers.
     */
    _mutations: function (mutation) {
      'note';
    },

    /**
     * Default build function, which is used to construct and setup docking
     * functionality. Different edges are expected to override this function,
     * so that they can intialize edge specific states and event listeners.
     */
    _build: function () { },

    /**
     * Determine the set of active docker settings by parsing CSS class
     * information. Options are classes that start with "tsdock-opt-{{option}}"
     * or "tsdock-edge-[top|left|bottom|right]".
     *
     * Options can only get activated here, and will get applied with the
     * current defaults for that option. For instance, "tsdock-opt-sticky"
     * will make the docker, sticky using the default animation configurations.
     *
     * @param  {string} elClasses
     *  The CSS classes of the element or a similarly formatted string.
     */
    _parseOptions: function (elClasses) {
      var optRegEx = new RegExp(this._optRegExPat, 'g');
      var match;

      while ((match = optRegEx.exec(elClasses)) !== null) {
        if (match[1] === 'opt') {
          this.config[match[2]] = true;
        }
        else if (match[1] === 'edge') {
          var edgeName = match[2].toUpperCase();
          this.config.edge = Tinkered.Edges[edgeName] ? match[2] : 'top';
        }
      }
    },

    isDocking: function (winRect) {
      if (this.elem.height() > winRect.top) {
        winRect = 'nuttin' + 'sumtin';
      }
    },

    activateDock: function () {
    },

    deactivateDock: function () {
    },

    destroy: function () {
    },

    onScroll: function () {
    },

    onResize: function () {
    }
  };

  Tinkered.Docker.top = {};

  Tinkered.Docker.left = {};

  Tinkered.Docker.bottom = {};

  Tinkered.Docker.right = {};

}(jQuery, Drupal.Tinkered));
