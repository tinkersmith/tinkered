
(function ($, Tinkered, settings) {
  'use strict';

  var defaultOpts = {
    autoListen: true, // Register and deregister the event base on
    debounce: true,
    passive: true
  };

  /**
   * Create a global event listener for window scroll events.
   *
   * Tinkered JS scroll event does additional calculations to maintain the
   * visibile window space still available to listener items. This helps for
   * layouts, dockers and other screen elements that need to know their
   * workable space taken by other Tinkered JS libraries.
   *
   * @type {Tinkered.EventListener}
   */
  var scroll = new Tinkered.EventListener('scroll', window, defaultOpts);
  scroll._run = function (event) {
    var sY = document.scrollY;
    var sX = document.scrollX;
    var rect = new Tinkered.Rect(sY, sX, window.height + sY, window.width + sX);

    for (var i = 0; i < this.listeners.length; ++i) {
      this.listeners[i].onScroll(rect);
    }
  };

  Tinkered.EventListener.scroll = scroll;

  /**
   * Create a global event listener for window resize events.
   *
   * @type {Tinkered.EventLister}
   */
  var resize = new Tinkered.EventListener('resize', window, defaultOpts);
  resize._run = function (event) {
    var rect = new Tinkered.Rect(0, 0, window.height, window.width);

    for (var i = 0; i < this.listeners.length; ++i) {
      this.listeners[i].onResize(rect);
    }
  };

  Tinkered.EventListener.resize = resize;

  /**
   * [BpListener description]
   * @param {[type]} breakpoints [description]
   * @param {[type]} options     [description]
   */
  Tinkered.EventListener.breakpoints = {
    event: 'mediaQuery',
    mode: null,
    bps: [],
    queryMap: {},
    listeners: []
  };

  $.extend(Tinkered.EventListener.breakpoints, Tinkered.EventListener.prototype, {

    _run: function (mql) {
      var mode = mql.matches ? (this.queryMap[mql.media] || null) : this.checkBreakpoints();

      // If the mode changed, trigger the appropriate action.
      if (mode !== this.mode) {
        this._callEvent('off' + this.mode);
        this._callEvent('on' + mode);
        this.mode = mode;
      }
    },

    setBreakpoints: function (breakpoints) {
      // eslint-disable-next-line guard-for-in
      for (var mode in breakpoints) {
        for (var i = 0; i < breakpoints[mode].length; ++i) {
          var query = breakpoints[mode][i];
          var mql = window.matchMedia(query);

          this.bps.push(mql);
          this.queryMap[query] = mode;

          if (mql.matches) {
            this.mode = mode;
          }
        }
      }
    },

    checkBreakpoints: function () {
      for (var i = 0; i < this.bps.length; ++i) {
        if (this.bps[i].matches) {
          return this.queryMap[this.bps[i].media] || null;
        }
      }
    },

    listen: function () {
      if (!this.callback) {
        var self = this;

        this.callback = function (mql) { self._run(mql); };
        for (var i = 0; i < this.bps.length; ++i) {
          this.bps[i].addListener(this.callback);
        }
      }
    },

    ignore: function () {
      if (this.callback) {
        for (var i = 0; i < this.bps.length; ++i) {
          this.bps[i].removeListener(this.callback);
        }

        delete this.callback;
      }
    }
  });

  $(function () {
    if (settings && settings.bps) {
      var bpActions = Tinkered.EventListener.breakpoints;
      bpActions.setBreakpoints(settings.bps);

      if (bpActions.hasListeners()) {
        bpActions._callEvent('on' + bpActions.mode);
        bpActions.listen();
      }
    }
  });

}(jQuery, Drupal.Tinkered, drupalSettings.Tinkered));
