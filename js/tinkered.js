
(function ($, Drupal, drupalSettings) {
  'use strict';

  // Define the Tinkered object scope.
  var Tinkered = Drupal.Tinkered = {

    /**
     * Simple escaping of RegExp strings.
     *
     * Does not handle advanced regular expressions, but will take care of
     * most cases. Meant to be used when concatenating string to create a
     * regular expressions.
     *
     * @param  {string} str
     *  String to escape.
     *
     * @return {string}
     *  String with the regular expression special characters escaped.
     */
    escapeRegex: function (str) {
      return str.replace(/[\^\$\+\*\?\[\]\{\}\(\)\\]/g, '\$&');
    },

    /**
     * Helper function to uppercase the first letter of a string.
     *
     * @param {string} str
     *  String to transform.
     *
     * @return {string}
     *  String which has the first letter uppercased.
     */
    ucFirst: function (str) {
      return str.charAt(0).toUpperCase() + str.slice(1);
    },

    /**
     * Transform a string into camel case. It will remove spaces, underscores
     * and hyphens, and uppercase the letter directly following them.
     *
     * @param {string} str
     *  The string to try to transform into camel case.
     *
     * @return {string}
     *  The string transformed into camel case.
     */
    camelCase: function (str) {
      return str.replace(/(?:[ \_\-]+)([a-z])/g, function (match, p1) {
        return p1.toUpperCase();
      });
    },

    /**
     * Transforms a string into Pascal case. This is basically the same as
     * camel case, except that it will upper case the first letter as well.
     *
     * @param {string} str
     *  The original string to transform into Pascal case.
     *
     * @return {string}
     *  The transformed string.
     */
    pascalCase: function (str) {
      return str.replace(/(?:^|[ \_\-]+)([a-z])/g, function (match, p1) {
        return p1.toUpperCase();
      });
    },

    /**
     * Determine if clean URLs are available or not, based on the current path.
     *
     * @return {bool}
     *  TRUE if the use of clean URLs can be determined.
     *  FALSE if clean URLs or we can't be sure if clean URLs are available.
     */
    useCleanURL: function () {
      // the result is stored in a static variable "isClean" for later reuse.
      if (typeof Tinkered.useCleanURL.isClean === 'undefined') {
        var url = /(\?|&)q=/.exec(window.location.href);

        // This test fails for frontpage, if not path is empty, but is still safe
        // because we default to generating a safe URL for either case.
        Tinkered.useCleanURL.isClean = (url == null && Tinkered.getCurrentPath().length > 0);
      }

      return Tinkered.useCleanURL.isClean;
    },

    /**
     * Gets the current page Drupal URL (excluding the query or base path).
     *
     * @return {string}
     *  The current internal path for Drupal. This would be the path
     *  without the "base path", however, it can still be a path alias.
     */
    getCurrentPath: function () {
      if (!Tinkered.getCurrentPath.path) {
        // can't use the useCleanURL() function, because it will cause a infinite recursive loop!
        var uri = /(\?|&)q=([^&]*)/.exec(window.location.href);
        var regex = new RegExp('^' + Tinkered.escapeRegex(Drupal.settings.basePath), 'i');

        Tinkered.utils.getCurrentPath.path = (uri === null || uri.length < 2)
          ? window.location.pathname.replace(regex, '') : uri[1];
      }

      return Tinkered.getCurrentPath.path;
    },

    /**
     * Parse URL query paramters from a URL.
     *
     * @param {string} url
     *  Full URL including the query parameters starting with '?' and
     *  separated with '&' characters.
     *
     * @return {Object}
     *  JSON formatted object which has the property names as the query
     *  key, and the property value is the query value.
     */
    getUrlParams: function (url) {
      var params = {};
      var pStr = (url || window.location.search).split('?');

      if (pStr.length > 1) {
        var parts = pStr[1].split('&');

        // eslint-disable-next-line guard-for-in
        for (var i in parts) {
          var matches = /^([^=]+)=(.*)$/.exec(parts[i]);

          if (matches && matches[1] !== 'q') {
            params[matches[1]] = decodeURIComponent(matches[2]);
          }
        }
      }

      return params;
    },

    /**
     * Build a URL based on a Drupal internal path. This function will test
     * for the availability of clean URL's and prefer them if available.
     * The URL components will be run through URL encoding.
     *
     * @param {string} rawUrl
     *  The URL to add the query parameters to. The URL can previously have
     *  query parameters already include. This will append additional params.
     * @param {Object|string} params
     *  An object containing parameters to use as the URL query. Object
     *  property keys are the query variable names, and the object property
     *  value is the value to use for the query.
     *
     * @return {string}
     *  The valid Drupal URL based on values passed.
     */
    buildURL: function (rawUrl, params) {
      var qStr = '';

      if (params) {
        if (typeof params === 'string') {
          qStr = params;
        }
        else {
          // eslint-disable-next-line guard-for-in
          for (var name in params) {
            qStr += '&' + encodeURIComponent(name) + '=' + encodeURIComponent(params[name]);
          }

          qStr = qStr.substr(1);
        }
      }

      // leave absolute URL's alone.
      if ((/^[a-z]{2,5}:\/\//i).test(rawUrl)) {
        if (qStr.length) {
          rawUrl += (rawUrl.indexOf('?') === -1 ? '?' : '&') + qStr;
        }

        return rawUrl;
      }

      // Clean the raw URL and prepare it for get fully assembled for Drupal.
      rawUrl = rawUrl ? rawUrl.replace(/^[\/,\s]+|<front>|([\/,\s]+$)/g, '') : '';
      var isCleanURL = Tinkered.useCleanURL() || rawUrl.length === 0;

      if (qStr.length > 0) {
        qStr = (isCleanURL ? '?' : '&') + qStr;
      }

      return Drupal.settings.basePath + (isCleanURL ? '' : '?q=') + rawUrl + qStr;
    },

    /**
     * Utility function used to find an object based on a string name.
     *
     * @param {string} name
     *  Fully qualified name of the object to fetch.
     *
     * @return {Object}
     *  the object matching the name, or NULL if it cannot be found.
     */
    getObject: function (name) {
      if (name && name.split) {
        return null;
      }

      var part;
      var ref = window;
      var parts = name.split('.');

      while ((part = parts.shift())) {
        if (typeof (ref = ref[part]) === 'undefined') {
          return null;
        }
      }

      return ref;
    },

    /**
     * Extend JS objects to implement subclassing and polymorphism.
     *
     * Allow us to properly extend JS classes, by copying the prototype.
     * This function properly inherits a prototype by creating a new
     * instance of the parent object first, so modifications to the subclass
     * do not effect the parent class.
     *
     * Also allow subclass to find the parent prototype, incase parent
     * methods need to be called from the subclass.
     *
     * @param {Object} subclass
     *  Function constructor which inherits it's prototype from parent
     * @param {Object} parent
     *  Base or super class.
     */
    inherit: function (subclass, parent) {
      var overrides = subclass.prototype;

      // we need to copy this prototype, by assigning it to a function
      // without parameters. Otherwise, we'll get initialization errors.
      var tmp = function () {};
      tmp.prototype = parent.prototype;
      subclass.prototype = new tmp();

      // Reapply all the prototype function overrides, if needed.
      if (overrides) {
        // eslint-disable-next-line guard-for-in
        for (var i in overrides) {
          subclass.prototype[i] = overrides[i];
        }
      }

      subclass.prototype.__construct = subclass;
      subclass.prototype.parent = parent.prototype;

      // ensure parent constructor is set.
      if (!parent.prototype.__construct) {
        parent.prototype.__construct = parent;
      }
    }
  };

  /**
   * Constructor for creating a set of event listeners. Window and global
   * events can be registered to global and registered to
   * Tinkered.EventListener.{eventName} namespace. Some of the events in that
   * namespace may have customized event callback.
   *
   * An example of this might be the ones defined in ./screen-events.js which
   * are used by the tinkered.dock.js and tinkered.layout.js.
   *
   * @param {DOMElement|jQUery} elem
   *  jQuery or DOM element that will be the target of the event.
   * @param {string} eventName
   *  The event.
   * @param {null|object} options
   *  method:
   *    Name of the method to call on all listeners (special cases). Will call
   *    the default "on[this.eventName]" method if left blank.
   *  useCapture:
   *    Use capture instead of bubbling for event propagation.
   *  passive:
   *    Event handlers will not call preventDefault() which can enable browser
   *    optimatization that no longer need to wait for all handlers to complete
   *    before triggering other events like scrolling.
   *  debounce:
   *    Determine if the event only triggers using debounce handling. This means
   *    that events will only fire off after a short delay.
   *
   *    If null or FALSE, no debounce will be used, and the event registered
   *    fires off as soon as the event is raised.
   *
   *    If TRUE then use the default debounce delay. If an integer, than use the
   *    value as the delay in milliseconds.
   */
  Tinkered.EventListener = function (elem, eventName, options) {
    options = options || {}; // options can be left blank.

    this.elem = elem instanceof $ ? elem[0] : elem;
    this.event = eventName;
    this.method = options.method || 'on' + Tinkered.pascalCase(eventName);
    this.autoListen = options.autoListen || false;
    this.listeners = [];

    // Check and properly organize the event options to be used later.
    if (options.debounce) {
      this.debounce = typeof options.debounce === 'boolean' ? 200 : options.debounce;
    }

    // Allow for addEventListener options as described here
    // https://developer.mozilla.org/en-US/docs/Web/API/EventTarget/addEventListener
    // I am also employing the https://github.com/WICG/EventListenerOptions
    // as a polyfill, but support will not be available for IE8 or older.
    this.eventOpt = {
      useCapture: options.useCapture || false,
      passive: options.passive || false
    };
  };

  Tinkered.EventListener.prototype = {

    /**
     * Trigger the event for all the registered listeners. Custom
     * EventListeners are most likely to override this function in order
     * to create implement special functionality, triggered by events.
     *
     * @param {Object} event
     *  The event object that was generated and passed to the event handler.
     */
    _run: function (event) {
      for (var i = 0; i < this.listeners.length; ++i) {
        this.listeners[this.method](event);
      }
    },

    /**
     * Call a method in all listeners, using the event provided. Unline
     * this._run() this method will check to make sure the listener supports
     * the event being requested.
     *
     * @param {string} method
     *  The name of the method to call from the listeners. Will check that
     *  this method exists before attempting to call.
     * @param {Object} event
     *  The original event object that was passed when event was triggered.
     */
    _callEvent: function (method, event) {
      for (var i = 0; i < this.listeners.length; ++i) {
        if (this.listener[method]) {
          this.listeners[method](event);
        }
      }
    },

    /**
     * Register the event, and keep track of the callback so it can be removed
     * later if we need to disable / remove the listener at a later time.
     */
    listen: function () {
      if (!this.callback) {
        var self = this;

        this.callback = function (event) { self._run(event); };
        this.elem.addEventListener(this.eventName, this.callback, this.eventOpts);
      }
    },

    /**
     * Stop listening for this event, and unregister from any event listeners.
     */
    ignore: function () {
      if (this.callback) {
        this.elem.removeEventListener(this.event, this.callback);
        delete this.callback;
      }
    },

    /**
     * If there is a valid atPos, place the listener at this position,
     * otherwise, just add it to the end of the list. This allows some
     * flexibility to place listeners at the start of the list, or
     * before other listeners.
     *
     * @param {Object} listener
     *  A listener object that contains the a method 'on' + [this.eventName].
     * @param {int} atPos
     *  Index to add the listener at. This allows listeners to be run in
     *  a different order than they maybe registered in.
     */
    add: function (listener, atPos) {
      // Ensure that all existing references to this event are removed.
      this.remove(listener);

      // eslint-disable-next-line no-unused-expressions
      (atPos && atPos >= 0) ? this.listeners.splice(atPos, 0, listener) : this.listeners.push(listener);

      // We can defer registering this listener until a listener is added.
      if (this.autoListen && !this.callback) {
        this.listen();
      }
    },

    /**
     * Add a new listener before an existing listener already in the list.
     * If [before] is null, then insert at the start of the list.
     *
     * @param {Object} listener
     *  A listener object that contains the a method 'on' + [this.eventName].
     * @param {Object} before
     *  Listener object that is used to position the new listener.
     */
    addBefore: function (listener, before) {
      var pos = before ? $.inArray(before, this.listeners) : 0;
      this.add(listener, pos >= 0 ? pos : null);
    },

    /**
     * Add a new listener after an existing listener already in the list.
     * If [after] is null, then insert at the end of the list.
     *
     * @param {Object} listener
     *  A listener object that contains the a method 'on' + [this.eventName].
     * @param {Object} after
     *  Listener object that is used to position the new listener.
     */
    addAfter: function (listener, after) {
      var pos = after ? $.inArray(after, this.listeners) : -1;
      this.add(listener, pos >= 0 ? pos + 1 : null);
    },

    /**
     * Remove the specified listener from the list of event listeners.
     * This assume there should only be one entry pert callback.
     *
     * @param {Object} listener
     *  A listener object that requests to get remoeved.
     */
    remove: function (listener) {
      var pos = 0;
      while ((pos = $.inArray(listener, this.listeners, pos)) >= 0) {
        this.listeners.splice(pos, 1);
      }

      // If there are no listeners and the autoListen option is on, turn off
      // listening for this event. This prevents the event from being called
      // for no reason.
      if (this.autoListen && !this.listeners.length) {
        this.ignore();
      }
    },

    /**
     * Clean-up event events and data.
     */
    destroy: function () {
      this.ignore();
    }
  };

  /**
   * Defines a enumerations for different edges.
   *
   * @type {Object}
   */
  Tinkered.Edges = {
    TOP: 0x01,
    LEFT: 0x10,
    BOTTOM: 0x02,
    RIGHT: 0x20
  };

  /**
   * Defines a directions enumerations or mask.
   *
   * @type {Object}
   */
  Tinkered.Direction = {
    VERTICAL: Tinkered.Edges.TOP | Tinkered.Edges.BOTTOM,
    HORIZONTAL: Tinkered.Edges.LEFT | Tinkered.Edges.RIGHT
  };
  Tinkered.Direction['ANY'] = Tinkered.Direction.HORIZONTAL | Tinkered.Direction.VERTICAL;

  // Ensure these are kept as constants,to use as enumerations.
  Object.freeze(Tinkered.Edges);
  Object.freeze(Tinkered.Direction);

}(jQuery, Drupal, drupalSettings));
